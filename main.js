// Importamos las librerías necesarias
const { app, BrowserWindow } = require('electron');
const path = require('path');
const isDev = require('electron-is-dev');

// Declaramos una variable para almacenar la ventana principal
let mainWindow;

// Función para crear la ventana principal
function createWindow() {
  // Creamos una nueva instancia de ventana de navegador
  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
    },
  });

  // Cargamos la URL en función del entorno de desarrollo o producción
  mainWindow.loadURL(
    isDev
      ? 'http://localhost:3000'
      : `file://${path.join(__dirname, '../build/index.html')}`
  );

  // Manejamos el evento de cierre de la ventana
  mainWindow.on('closed', () => {
    mainWindow = null;
  });
}

// Creamos la ventana cuando la aplicación esté lista
app.on('ready', createWindow);

// Cerramos la aplicación cuando todas las ventanas estén cerradas (excepto en macOS)
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

// Creamos la ventana si se activa la aplicación y no hay ventanas abiertas (macOS)
app.on('activate', () => {
  if (mainWindow === null) {
    createWindow();
  }
});
